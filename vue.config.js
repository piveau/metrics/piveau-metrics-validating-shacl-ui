module.exports = {
  publicPath: '/',
  devServer: {
    host: 'localhost',
  },
  runtimeCompiler: true,
  transpileDependencies: ['vuetify'],
};
